module gb-fitness

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/text v0.3.5 // indirect
	gopkg.in/yaml.v2 v2.4.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.12

)
