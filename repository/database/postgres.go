package database

import (
	"gb-fitness/models"

	"gorm.io/gorm"
)

type PG struct {
	db *gorm.DB
}

type Database interface {
	FindAllWorkouts(workouts *[]models.Workout) error
	CreatePerson(person *models.Person) error
	SignUpWorkout(workout *models.Workout) error
	FindWorkoutByID(workout *models.Workout, id int) error
	ModifiedWorkout(workout *models.Workout) error
	CreateWorkout(workout *models.Workout) error
	DeleteWorkoutByID(w *models.Workout, id int) error
}

func NewPG(db *gorm.DB) *PG {
	return &PG{
		db: db,
	}
}

// FindAllWorkouts method
func (pg *PG) FindAllWorkouts(workouts *[]models.Workout) error {
	if err := pg.db.Find(workouts).Error; err != nil {
		return err
	}
	return nil
}

// CreatePerson method
func (pg *PG) CreatePerson(person *models.Person) error {
	if err := pg.db.Create(person).Error; err != nil {
		return err
	}
	return nil
}

// Sing Up workout method
func (pg *PG) SignUpWorkout(workout *models.Workout) error {
	if err := pg.db.Create(workout).Error; err != nil {
		return err
	}
	return nil
}

// FindWorkoutByID method
func (pg *PG) FindWorkoutByID(workout *models.Workout, id int) error {
	if err := pg.db.Where("ID = ?", id).First(workout).Error; err != nil {
		return err
	}
	return nil
}

// ModifiedWorkout method
func (pg *PG) ModifiedWorkout(workout *models.Workout) error {
	if err := pg.db.Save(workout).Error; err != nil {
		return err
	}
	return nil
}

// CreateWorkout method
func (pg *PG) CreateWorkout(workout *models.Workout) error {
	if err := pg.db.Create(workout).Error; err != nil {
		return err
	}
	return nil
}

// DeleteWorkoutByID method
func (pg *PG) DeleteWorkoutByID(w *models.Workout, id int) error {
	if err := pg.db.Delete(w, id).Error; err != nil {
		return err
	}
	return nil
}
