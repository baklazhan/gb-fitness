package main

import (
	"flag"

	"log"

	"gb-fitness/config"
	"gb-fitness/server"
)

func main() {

	flagConfigFile := flag.String("config", "config/default.yaml", "Flag config in yaml format")
	flag.Parse()

	config, err := config.ReadConfig(*flagConfigFile)
	if err != nil {
		log.Fatal(err)
	}

	server := server.NewServer(&config.Server)
	if err := server.Start(); err != nil {
		log.Fatal(err)
	}
}
