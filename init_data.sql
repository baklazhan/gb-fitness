INSERT INTO "people" (id, "firstname", "lastname", "coach") VALUES (1, 'Лена', 'Иванова', true);
-- INSERT INTO "people" (id, "firstname", "lastname", "coach") VALUES (2, 'Юля', 'Лазарева', false);
-- INSERT INTO "people" (id, "firstname", "lastname", "coach") VALUES (3, 'Катя', 'Петрова', false);

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-09 12:00:00.000000', '2021-08-09 15:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-09 15:00:00.000000', '2021-08-09 18:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-09 18:00:00.000000', '2021-08-09 21:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-10 12:00:00.000000', '2021-08-10 15:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-10 15:00:00.000000', '2021-08-10 18:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-10 18:00:00.000000', '2021-08-10 21:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-11 12:00:00.000000', '2021-08-11 15:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-11 12:00:00.000000', '2021-08-11 18:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-11 12:00:00.000000', '2021-08-11 21:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-12 12:00:00.000000', '2021-08-12 15:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-12 12:00:00.000000', '2021-08-12 18:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-12 12:00:00.000000', '2021-08-12 21:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-13 12:00:00.000000', '2021-08-13 15:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-13 12:00:00.000000', '2021-08-13 18:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-13 12:00:00.000000', '2021-08-13 21:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-14 12:00:00.000000', '2021-08-14 15:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-14 12:00:00.000000', '2021-08-14 18:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-14 12:00:00.000000', '2021-08-14 21:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-15 12:00:00.000000', '2021-08-15 15:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-15 12:00:00.000000', '2021-08-15 18:00:00.000000');

INSERT INTO "workouts" (id, "coach_id", "person_id", "person_lastname", "start_time", "end_time")
VALUES (default, 1, null, null, '2021-08-15 12:00:00.000000', '2021-08-15 21:00:00.000000');
