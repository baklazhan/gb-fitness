package server

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gb-fitness/controllers"
	"gb-fitness/models"
	"gb-fitness/repository/database"

	"github.com/gorilla/mux"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type ConfigServer struct {
	Address string `yaml:"address"`
	DSN     string `yaml:"dsn"`
}

type Server struct {
	httpServer *http.Server
	db         *gorm.DB
	config     *ConfigServer
}

func NewServer(config *ConfigServer) *Server {
	db := initDB(config.DSN)
	return &Server{
		db:     db,
		config: config,
	}
}

func (server *Server) Start() error {
	controllers.InitTemplates()

	pg := database.NewPG(server.db)
	controllers.ConnectDB(pg)

	router := mux.NewRouter()
	router.HandleFunc("/", controllers.MainHandler)
	router.HandleFunc("/trainers", controllers.CoachHandler)
	router.HandleFunc("/trainers/create_workout", controllers.CreateWorkoutFormHandler).Methods(http.MethodGet)
	router.HandleFunc("/trainers/create_workout", controllers.CreateWorkoutHandler).Methods(http.MethodPost)
	router.HandleFunc("/workout/{id:[0-9]+}", controllers.SignUpWorkoutFormHandler).Methods(http.MethodGet)
	router.HandleFunc("/workout/{id:[0-9]+}", controllers.SignUpWorkoutHandler).Methods(http.MethodPost)
	router.HandleFunc("/workout/delete/{id:[0-9]+}", controllers.DeleteWorkoutHandler)

	server.httpServer = &http.Server{
		Addr:           server.config.Address,
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	go func() {
		if err := server.httpServer.ListenAndServe(); err != nil {
			log.Fatalf("Failed to listen and serve: %+v", err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, os.Interrupt)

	<-quit

	ctx, shutdown := context.WithTimeout(context.Background(), 5*time.Second)
	defer shutdown()

	return server.httpServer.Shutdown(ctx)
}

func initDB(dsn string) *gorm.DB {
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Error occured while establishing connection to Postgres")
	}

	db.AutoMigrate(&models.Person{})
	db.AutoMigrate(&models.Workout{})

	workout := models.Workout{}

	if rows := db.Take(&workout).RowsAffected; rows == 0 {

		workouts := []models.Workout{
			{StartTime: time.Date(2021, time.August, 30, 12, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.August, 30, 15, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.August, 30, 15, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.August, 30, 18, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.August, 30, 18, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.August, 30, 21, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.August, 31, 12, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.August, 31, 15, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.August, 31, 15, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.August, 31, 18, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.August, 31, 18, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.August, 31, 21, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.September, 1, 12, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.September, 1, 15, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.September, 1, 15, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.September, 1, 18, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.September, 1, 18, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.September, 1, 21, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.September, 2, 12, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.September, 2, 15, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.September, 2, 15, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.September, 2, 18, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.September, 2, 18, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.September, 2, 21, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.September, 3, 12, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.September, 3, 15, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.September, 3, 15, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.September, 3, 18, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.September, 3, 18, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.September, 3, 21, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.September, 4, 12, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.September, 4, 15, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.September, 4, 15, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.September, 4, 18, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.September, 4, 18, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.September, 4, 21, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.September, 5, 12, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.September, 5, 15, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.September, 5, 15, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.September, 5, 18, 0, 0, 0, time.Local)},
			{StartTime: time.Date(2021, time.September, 5, 18, 0, 0, 0, time.Local),
				EndTime: time.Date(2021, time.September, 5, 21, 0, 0, 0, time.Local)},
		}

		if err := db.Create(workouts).Error; err != nil {
			log.Println("Error occured while creating default workouts:", err)
		}
	}
	return db
}
